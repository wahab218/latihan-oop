<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal = new Animal("shaun");
echo "Name : " . $animal->Name . "<br>";
echo "legs : " . $animal->legs . "<br>";
echo "cold blooded : " . $animal->cold_blooded . "<br>";
echo "<br>";

$frog = new frog("buduk");
echo "Name : " . $frog->Name . "<br>";
echo "legs : " . $frog->legs . "<br>";
echo "cold blooded : " . $frog->cold_blooded . "<br>";
echo $frog->jump("Hop Hop");
echo "<br>";

$ape = new ape("kera sakti");
echo "Name : " . $ape->Name . "<br>";
echo "legs : " . $ape->legs . "<br>";
echo "cold blooded : " . $ape->cold_blooded . "<br>";
echo $ape->yell("Auoo");

?>